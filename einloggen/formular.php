<?php session_start ();
include $_SERVER['DOCUMENT_ROOT']."/config/config.php";
include $_SERVER['DOCUMENT_ROOT']."/include/footer.php";
?>
<html>
<head>
  <link href="./style.css" rel="stylesheet" type="text/css" />
  <link href="/<?php echo $applroot?>/css/style.css" rel="stylesheet" type="text/css">
  <!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script  src="./script.js"></script> -->
  <title>Login</title>
</head>

<body>
<?php
if (isset ($_REQUEST["fehler"]))
{
  echo "Die Zugangsdaten waren ungültig.";
}
?>

<h1>SIR E-SPORT Admin Portal</h1>
<div class="demo-table">
<div class="form-head">Login</div>
  <form action="/einloggen/login.php" method="post">
    Name: <br><input class="name" type="text" name="name" size="20"><br>
    Kennwort:<br> <input class="pw" type="password" name="pwd" size="20"><br>
    <input class="buttona" type="submit" value="Login">
  </form>
  <div class="copy">
    © by Noel
  </div>
</div>
