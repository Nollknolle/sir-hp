<?php
include ("checkuser.php");
include $_SERVER['DOCUMENT_ROOT']."/config/config.php";
?>
<html>
<head>
  <meta http-equiv="refresh" content="3; URL=/player/index.php">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script>
  <script  src="./script.js"></script>
  <title>Interne Seite</title>
</head>
  <body>
  <script type="text/javascript">
  Swal.fire({
  position: 'top-end',
  type: 'success',
  title: 'Hallo [ <?php echo $_SESSION["user_kuerzel"]; ?> ]\n\nWillkommen <?php echo $_SESSION["user_vorname"]; ?> <?php echo $_SESSION["user_nachname"]; ?>.',
  showConfirmButton: false,
  timer: 3000
  })  </script>
  </body>

</html>
