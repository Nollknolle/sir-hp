<?php
    include $_SERVER['DOCUMENT_ROOT']."/einloggen/checkuser.php";
    include $_SERVER['DOCUMENT_ROOT']."/config/config.php";
    include $_SERVER['DOCUMENT_ROOT']."/include/footer.php";
    include $_SERVER['DOCUMENT_ROOT']."/include/menu.php";
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" href="/css/style.css" type="text/css" /> 
    <link rel="stylesheet" href="/css/menu.css" type="text/css" /> 
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
    <meta charset="UTF-8">
    <title>SIR eSports Admin Portal</title>
</head>
<body>

  <?php
    include $_SERVER['DOCUMENT_ROOT']."/include/header.php";
  ?>
  
  <h1>Spieler</h1>
  <h2>Aktuelle Spieler f&uuml;r SIR</h2>
  <br />

	<?php  
	$conn = new mysqli($servername, $username, $passwort, "noel");

    if ($conn->connect_error) {
    	die("Connection failed: " . $conn->connect_error);
	}
	
  $sql = 'SELECT * FROM `spieler` ORDER BY `team` ASC, `team` ASC';
  
  $rechte = "SELECT ".
    					"ix, Kuerzel, loginname, Vorname, Kuerzel ".
  						"FROM ".
    					"mitarbeiter".
  						" WHERE ".
    					"(loginname like '".$_SESSION["user_nickname"]."') AND ".
						"(rechte like 'admin')";
						
		$result_rechte = mysqli_query($conn, $rechte);

	$result = mysqli_query( $conn, $sql );
	if ( ! $result ){
		die("Ungueltige Abfrage: ");
	}

	echo '<table align="center">';
	echo "<tr><th width='150'>Spieler ID</th><th width='150'>Vorname</th><th width='150'>Nachname</th><th width='50'>Mail</th><th width='300'>Telefon</th><th width='100'>DSGVO</th><th width='150'>Team</th>";
	while ($zeile = mysqli_fetch_array( $result)){
		echo "<tr>";
    echo "<td style='text-align: center;'>". $zeile['ix'] . "</td>";
		echo "<td style='text-align: center;'>". $zeile['vorname'] . "</td>";
		echo "<td style='text-align: center;'>". $zeile['nachname'] . "</td>";
		if (mysqli_num_rows($result_rechte) > 0)
			{
				// Benutzerdaten in ein Array auslesen.
				echo "<td style='text-align: center;'>". $zeile['mail'] . "</td>";
				
			}
			else
				{
          echo "<td style='text-align: center;'><img src='/img/nicht-verfügbar.png' alt='Keine Anzeigerechte' title='Keine Anzeigerechte! \nBitte beantragen Sie mehr Rechte beim Administrator.'></td>";
				}
      if (mysqli_num_rows($result_rechte) > 0)
				{
				  // Benutzerdaten in ein Array auslesen.
					echo "<td style='text-align: center;'>". $zeile['telefon'] . "</td>";
				
				}
				else
					{
					echo "<td style='text-align: center;'><img src='/img/nicht-verfügbar.png' alt='Keine Anzeigerechte' title='Keine Anzeigerechte! \nBitte beantragen Sie mehr Rechte beim Administrator.'></td>";
					}
		echo '<td style="text-align: center;"> <img src="'.$dsgvo_status_png[$zeile["DSGVO"]].'" alt="Ampel"/></td>';
		echo "<td style='text-align: center;'>". $zeile['team'] . "</td>";		

    ?>
    <td>
    <button <?php if($zeile['DSGVO']==1){echo "disabled";}?> title="DSGVO akzeptiert" OnClick="location.href='/player/edit/edit_dsgvo_y.php?ix=<?php echo $zeile['ix'] ?>'">
    <img src="/img/status-green-15.png" alt="L�schen"/>
    </button>
    </td>
    
    <td>
    <button <?php if($zeile['DSGVO']==0){echo "disabled";} ?> title="DSGVO abgelehnt" OnClick="location.href='/player/edit/edit_dsgvo_n.php?ix=<?php echo $zeile['ix'] ?>'">
    <img src="/img/status-red-15.png" alt="L�schen"/>
    </button>
    </td>
    
    <td>
    <p> | </p>
    </td>
    
    <td>
    <button title="Spieler l&ouml;schen" OnClick="location.href='/player/edit/edit_delete.php?ix=<?php echo $zeile['ix'] ?>'">
    <img src="/img/loeschen.png" alt="L�schen"/>
    </button>
    </td>
	<?php
 		echo "<tr>";
     }
	echo "</table>";
 

   

	mysqli_free_result( $result );

	mysqli_close ($conn)
	?>

</body>
</html>
