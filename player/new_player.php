

<?php
    include $_SERVER['DOCUMENT_ROOT']."/einloggen/checkuser.php";
    include $_SERVER['DOCUMENT_ROOT']."/config/config.php";
    include $_SERVER['DOCUMENT_ROOT']."/include/footer.php";
    include $_SERVER['DOCUMENT_ROOT']."/include/menu.php";
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" href="/css/style.css" type="text/css" /> 
    <link rel="stylesheet" href="/css/menu.css" type="text/css" /> 
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
    <meta charset="UTF-8">
    <title>SIR eSports Admin Portal</title>
    <style>
    .body{
        text-align: center;
    }
    b{
        color: #fff;
    }
    </style>
</head>
<body>

  <?php
    include $_SERVER['DOCUMENT_ROOT']."/include/header.php";
  ?>
  <div class="body">
  <h2>Neuer Spieler</h2>
  <h3><u>Bitte auf Rechtschreibung achten!</u></h3>

    <div class="demo-table">
	  <form action="new.php" method="post">
	    <b>Vorname:</b><br><input required type="text" placeholder="Vorname" name="vorname"><br>
	    <b>Nachname:</b><br><input required type="text" placeholder="Nachname" name="nachname"><br>
        <b>Mail:</b><br><input required type="text" placeholder="Mail" name="mail"><br>
        <b>Telefon:</b><br><input required type="text" placeholder="Telefon" name="telefon"><br>
        <b>Team:</b><br><input required type="text" placeholder="Team" name="team"><br><br>
	    <input style="padding:10px; height:30px;" class="buttona" type="submit" value="Abschicken">
	  </form>
    </div>
  </div>
  
</body>
</html>
