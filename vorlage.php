<?php
    include $_SERVER['DOCUMENT_ROOT']."/einloggen/checkuser.php";
    include $_SERVER['DOCUMENT_ROOT']."/config/config.php";
    include $_SERVER['DOCUMENT_ROOT']."/include/footer.php";
    include $_SERVER['DOCUMENT_ROOT']."/include/menu.php";

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" href="/css/style.css" type="text/css" /> 
    <link rel="stylesheet" href="/css/menu.css" type="text/css" /> 
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
    <meta charset="UTF-8">
    <title>SIR eSports Admin Portal</title>
</head>
<body>

  <?php
    include $_SERVER['DOCUMENT_ROOT']."/include/header.php";
  ?>

</body>
</html>
